/*
  QQE03-12SPUD
  Version 2.1.0
  Date: 26.12.2016
*/
#include <EasyTransfer.h>
#include <AmpTransfer.h>
#include <Blink.h>
#include <OneWire.h>
#include <DallasTemperature.h>

static const byte QQE0312SPUD_ID = 122;
static const byte ampId = QQE0312SPUD_ID;

// Pin Config
#define regulatorsPin 3
#define oneWireBusPin 5
#define relayPin 6
#define ledRedPin 9
#define ledGreenPin 10
#define ledBluePin 11
#define leftCurrentA1Pin A3
#define leftCurrentA2Pin A2
#define rightCurrentA1Pin A6
#define rightCurrentA2Pin A7

// Constants
#define startCurrent 30
#define minCurrent 200 // 0.04 mA per Units
#define maxCurrent 850 // 0.04 mA per Units
#define minRefCurrent 360 // 0.04 mA per Units
#define maxRefCurrent 670 // 0.04 mA per Units
#define dischargeMinTime 1 // Seconds
#define dischargeMaxTime 5 // Seconds
#define heatMaxTime 35 // Seconds.
#define highVoltageMaxTime 2 // seconds. Time starting the high voltage before the regulator begin to stabilize
#define regulationMaxTime 30 // seconds. Time required to the regulator to stabilize the current
#define errorMaxTime 500 // Milli-seconds
#define currentAverageRatio 100
#define stabilizedTreshold 60 // 0.04 mA per Units
#define modulationPeakReductionFactor 0.02
#define modulationReductionTime 200 // Milli-seconds
#define heatThinkTempMax 85 // > 85deg
#define airTempMax 70 // > 70deg
#define tempMeasureMinTime 10000 // Milli-seconds
#define modulationPeakAverageRatio 10
#define ledDim 40 // 0-100

// Internal use
Blink ledRed;
Blink ledGreen;
Blink ledBlue;
OneWire oneWire(oneWireBusPin);
DallasTemperature tempSensors(&oneWire);
unsigned long lastTempMeasureTime = 0;
double leftA1Current = 0;
double leftA2Current = 0;
double rightA1Current = 0;
double rightA2Current = 0;
double leftA1CurrentAverage = 0;
double leftA2CurrentAverage = 0;
double rightA1CurrentAverage = 0;
double rightA2CurrentAverage = 0;
double modulationPeakAverage = 0;
double modulationPeak = 0;
unsigned long modulationReductionLastTime = 0;
unsigned int percentageSetPoint = 0;
unsigned int dischargeTime = 0;
unsigned long heatStartTime;
unsigned int heatTime = 0;
unsigned long highVoltageStartTime = 0;
unsigned int highVoltageTime = 0;
unsigned long stabilizationStartTime = 0;
unsigned int stabilizationTime = 0;
unsigned int stepMaxTime = 0;
unsigned int stepElapsedTime = 0;
unsigned int stepMaxValue = 0;
unsigned int stepCurValue = 0;
unsigned int airTemp = 0;
unsigned int powerSupply1Temp = 0;
unsigned int powerSupply2Temp = 0;
unsigned long sequenceStartTime = 0;
unsigned int refCurrent = 0;
unsigned long errorTime = 0;
String inputString = "";
String request = "";

// Timers definition
#define TIMER_CLK_DIV1 0x01 // Timer clocked at F_CPU
#define TIMER_PRESCALE_MASK 0x07

// Temp indexes in one wire bus
#define AIR_TEMPERATURE 2
#define POWERSUPPLY1_TEMPERATURE 0
#define POWERSUPPLY2_TEMPERATURE 1

// Sequence:
byte sequence = SEQUENCE_DISCHARGE;

// Errors
byte errorNumber = NO_ERROR;

#define NO_ERROR_TUBE 0;
#define ERROR_TUBE_LEFT_1 1;
#define ERROR_TUBE_LEFT_2 2;
#define ERROR_TUBE_RIGHT_1 3;
#define ERROR_TUBE_RIGHT_2 4;
#define ERROR_TEMP_AIR 1
#define ERROR_TEMP_PWR1 2
#define ERROR_TEMP_PWR2 3
byte errorCause = NO_ERROR_TUBE;

#define CHECK_RANGE_OK 0
#define CHECK_RANGE_TOOLOW 1
#define CHECK_RANGE_TOOHIGH 2

// Diagnostic
EasyTransfer dataTx;
dataResponse dataTxStruct;

byte checkInRange(double minValue, double maxValue)
{
  if (minValue > 0) {
    if (leftA1CurrentAverage < minValue)
    {
      errorCause = ERROR_TUBE_LEFT_1;
      return CHECK_RANGE_TOOLOW;
    }
    if (leftA2CurrentAverage < minValue)
    {
      errorCause = ERROR_TUBE_LEFT_2;
      return CHECK_RANGE_TOOLOW;
    }
    if (rightA1CurrentAverage < minValue)
    {
      errorCause = ERROR_TUBE_RIGHT_1;
      return CHECK_RANGE_TOOLOW;
    }
    if (rightA2CurrentAverage < minValue)
    {
      errorCause = ERROR_TUBE_RIGHT_2;
      return CHECK_RANGE_TOOLOW;
    }
  }

  if (maxValue > 0) {
    if (leftA1CurrentAverage > maxValue)
    {
      errorCause = ERROR_TUBE_LEFT_1;
      return CHECK_RANGE_TOOHIGH;
    }
    if (leftA2CurrentAverage > maxValue)
    {
      errorCause = ERROR_TUBE_LEFT_2;
      return CHECK_RANGE_TOOHIGH;
    }
    if (rightA1CurrentAverage > maxValue)
    {
      errorCause = ERROR_TUBE_RIGHT_1;
      return CHECK_RANGE_TOOHIGH;
    }
    if (rightA2CurrentAverage > maxValue)
    {
      errorCause = ERROR_TUBE_RIGHT_2;
      return CHECK_RANGE_TOOHIGH;
    }
  }

  errorCause = NO_ERROR;
  return CHECK_RANGE_OK;
}

void sendDatas()
{
  // Send datas
  dataTxStruct.step = sequence;
  dataTxStruct.stepMaxTime = stepMaxTime;
  dataTxStruct.stepElapsedTime = stepElapsedTime;
  dataTxStruct.stepMaxValue = stepMaxValue;
  dataTxStruct.stepCurValue = stepCurValue;
  dataTxStruct.tickCount = millis();
  dataTxStruct.measure0 = map((int)leftA1CurrentAverage, 0, 1024, 0, 255); // Input 1024 max, but only a range of 255 is transfered
  dataTxStruct.measure1 = map((int)leftA2CurrentAverage, 0, 1024, 0, 255);
  dataTxStruct.measure2 = map((int)rightA1CurrentAverage, 0, 1024, 0, 255);
  dataTxStruct.measure3 = map((int)rightA2CurrentAverage, 0, 1024, 0, 255);
  dataTxStruct.measure4 = map(leftA1Current, 0, 1024, 0, 255);
  dataTxStruct.measure5 = map(leftA2Current, 0, 1024, 0, 255);
  dataTxStruct.measure6 = map(rightA1Current, 0, 1024, 0, 255);
  dataTxStruct.measure7 = map(rightA2Current, 0, 1024, 0, 255);
  dataTxStruct.output0 = map(modulationPeakAverage, 0, 1024, 0, 255);
  dataTxStruct.output1 = dataTxStruct.output0;
  dataTxStruct.output2 = map(modulationPeak, 0, 1024, 0, 255);
  dataTxStruct.output3 = dataTxStruct.output2;
  dataTxStruct.temperature0 = constrain(airTemp, 0, 255);
  dataTxStruct.temperature1 = constrain(powerSupply1Temp, 0, 255);
  dataTxStruct.temperature2 = constrain(powerSupply2Temp, 0, 255);
  dataTxStruct.minValue = map(minCurrent, 0, 1023, 0, 255);
  dataTxStruct.refValue = map(refCurrent, 0, 1023, 0, 255);
  dataTxStruct.maxValue = map(maxCurrent, 0, 1023, 0, 255);
  dataTxStruct.errorNumber = errorNumber;
  dataTxStruct.errorTube = errorCause;
  dataTx.sendData();
}

void measureTemperatures()
{
  // Send the command to get temperatures
  tempSensors.requestTemperatures();
  airTemp = tempSensors.getTempCByIndex(AIR_TEMPERATURE);
  powerSupply1Temp = tempSensors.getTempCByIndex(POWERSUPPLY1_TEMPERATURE);
  powerSupply2Temp = tempSensors.getTempCByIndex(POWERSUPPLY2_TEMPERATURE);
  lastTempMeasureTime = millis();
}

void relayOn(void)
{
  analogWrite(relayPin, 127);
}

void relayOff(void)
{
  analogWrite(relayPin, 0);
}

unsigned int calcPercentSetPoint() {
  if (modulationPeak > 270) {
    return 100;
  }

  if (modulationPeak > 132) {
    return 50;
  }

  // Note that 0 is the null value
  return 1;
}

// the setup routine runs once when you press reset:
void setup() {
  // initialize the digital pin as an output.
  pinMode(relayPin, OUTPUT);
  pinMode(ledRedPin, OUTPUT);
  pinMode(ledGreenPin, OUTPUT);
  pinMode(ledBluePin, OUTPUT);

  // Set PWM speed
  TCCR1B = (TCCR1B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;
  TCCR2B = (TCCR2B & ~TIMER_PRESCALE_MASK) | TIMER_CLK_DIV1;

  digitalWrite(oneWireBusPin, HIGH);

  ledRed.Setup(ledRedPin, true, ledDim);
  ledGreen.Setup(ledGreenPin, true, ledDim);
  ledBlue.Setup(ledBluePin, true, ledDim);

  tempSensors.begin();
  tempSensors.requestTemperatures();
  measureTemperatures();

  // Diagnostic
  Serial.begin(2400);
  dataTxStruct.id = ampId;
  dataTx.begin(details(dataTxStruct), &Serial);

  inputString.reserve(200);
}

// the loop routine runs over and over again forever:
void loop()
{
  unsigned int elapsedTime;
  unsigned int check;
  unsigned long currentTime = millis();

  if (sequence != SEQUENCE_FAIL) {
    if (airTemp > airTempMax) {
      // Fail, air temperature too high
      sequence = SEQUENCE_FAIL;
      errorNumber = ERROR_TEMPTOOHIGH;
      errorCause = ERROR_TEMP_AIR;
    }
    else if (powerSupply1Temp > heatThinkTempMax) {
      // Fail, regulators 1 temperature too high
      sequence = SEQUENCE_FAIL;
      errorNumber = ERROR_TEMPTOOHIGH;
      errorCause = ERROR_TEMP_PWR1;
    }
    else if (powerSupply2Temp > heatThinkTempMax) {
      // Fail, regulators 2 temperature too high
      sequence = SEQUENCE_FAIL;
      errorNumber = ERROR_TEMPTOOHIGH;
      errorCause = ERROR_TEMP_PWR2;
    }
    else {
      // Read and smooth the input
      leftA1Current = 1023 - min(analogRead(leftCurrentA1Pin), 1023);
      leftA2Current = 1023 - min(analogRead(leftCurrentA2Pin), 1023);
      rightA1Current = 1023 - min(analogRead(rightCurrentA1Pin), 1023);
      rightA2Current = 1023 - min(analogRead(rightCurrentA2Pin), 1023);

      leftA1CurrentAverage += (leftA1Current - leftA1CurrentAverage) / currentAverageRatio;
      leftA2CurrentAverage += (leftA2Current - leftA2CurrentAverage) / currentAverageRatio;
      rightA1CurrentAverage += (rightA1Current - rightA1CurrentAverage) / currentAverageRatio;
      rightA2CurrentAverage += (rightA2Current - rightA2CurrentAverage) / currentAverageRatio;

      if (sequence == SEQUENCE_FUNCTION) {
        unsigned int leftmod = max(abs(leftA1Current - leftA1CurrentAverage), abs(leftA2Current  - leftA2CurrentAverage));
        unsigned int rightmod = max(abs(rightA1Current - rightA1CurrentAverage), abs(rightA2Current  - rightA2CurrentAverage));
        modulationPeakAverage +=  (max(leftmod, rightmod) - modulationPeakAverage) / modulationPeakAverageRatio;
        if (modulationPeak < modulationPeakAverage)
        {
          modulationPeak = modulationPeakAverage;
        }
      }

      if (sequence >= SEQUENCE_REGULATING) {
        // Modulation reduction factor
        if (currentTime - modulationReductionLastTime > modulationReductionTime) {
          if (modulationPeak > modulationPeakReductionFactor) {
            modulationPeak -= modulationPeakReductionFactor;
          }
          else {
            modulationPeak = 0;
          }
          modulationReductionLastTime = currentTime;
        }

        // Calc regulators set point
        unsigned int percentage = calcPercentSetPoint();
        if (percentageSetPoint != percentage) {
          // New set point must be set
          refCurrent = minRefCurrent + percentage * (maxRefCurrent - minRefCurrent) / 100;
          percentageSetPoint = percentage;
        }
      }

      analogWrite(regulatorsPin, map(refCurrent, 0, 1024, 0, 255));
    }
  }

  if (sequence != SEQUENCE_STARTING && sequence != SEQUENCE_REGULATING && currentTime - lastTempMeasureTime > tempMeasureMinTime) {
    // Measure Temp
    measureTemperatures();
  }

  switch (sequence)
  {
    case SEQUENCE_DISCHARGE:
      // Discharging

      // Reset errors
      errorCause = NO_ERROR;
      errorNumber = NO_ERROR;

      // Pre-sequence
      if (sequenceStartTime == 0) {
        relayOff();
        sequenceStartTime = currentTime;
        elapsedTime = 0;
        ledRed.Off();
        ledBlue.Off();
      }
      else {
        // Calc elapsed time in seconds
        elapsedTime = (currentTime - sequenceStartTime) / 1000;
      }

      ledGreen.Execute(800, 200);

      // Diagnostic
      stepMaxTime = dischargeMaxTime;
      stepElapsedTime = elapsedTime;
      stepMaxValue = 0;
      stepCurValue = 1;

      if (elapsedTime > dischargeMaxTime)
      {
        // Fail, too late
        sequence = SEQUENCE_FAIL;
        errorNumber = ERROR_DISHARGETOOLONG;
        break;
      }

      if (elapsedTime < dischargeMinTime || checkInRange(0, startCurrent) == CHECK_RANGE_TOOHIGH)
      {
        break;
      }

      // Post-sequence
      sequenceStartTime = 0;
      sequence = SEQUENCE_HEAT;

    case SEQUENCE_HEAT:
      // Startup tempo

      // Pre-sequence
      if (sequenceStartTime == 0) {
        sequenceStartTime = currentTime;
        elapsedTime = 0;
        ledRed.Off();
        ledBlue.Off();
      }
      else {
        // Calc elapsed time in seconds
        elapsedTime = (currentTime - sequenceStartTime) / 1000;
      }

      ledGreen.Execute(400, 400);

      // Diagnostic
      stepMaxTime = heatMaxTime;
      stepElapsedTime = elapsedTime;
      stepMaxValue = heatMaxTime;
      stepCurValue = elapsedTime;

      // Ensure no current at this step
      if (checkInRange(0, startCurrent + 10) == CHECK_RANGE_TOOHIGH)
      {
        // Fail, no current allowed now
        sequence = SEQUENCE_FAIL;
        errorNumber = ERROR_CURRENTONHEAT;
        break;
      }

      if (elapsedTime < heatMaxTime)
      {
        break;
      }

      // Diagnostic, force 100%
      stepElapsedTime = heatMaxTime;
      stepCurValue = heatMaxTime;

      // Post-sequence
      sequenceStartTime = 0;
      ledGreen.On();
      ledRed.On();
      measureTemperatures();
      sequence = SEQUENCE_STARTING;
      delay(250);

    case SEQUENCE_STARTING:
      // Starting High Voltage

      // Pre-sequence
      if (sequenceStartTime == 0) {
        relayOn();
        sequenceStartTime = currentTime;
        elapsedTime = 0;
        ledRed.Off();
        ledBlue.Off();
      }
      else {
        // Calc elapsed time in seconds
        elapsedTime = (currentTime - sequenceStartTime) / 1000;
      }

      ledGreen.Execute(20, 400);

      // Diagnostic
      stepMaxTime = highVoltageMaxTime;
      stepElapsedTime = elapsedTime;
      stepMaxValue = highVoltageMaxTime;
      stepCurValue = elapsedTime;

      if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
      {
        // Fail current error
        sequence = SEQUENCE_FAIL;
        errorNumber = ERROR_STARTINGOUTOFRANGE;
        break;
      }

      if (elapsedTime < highVoltageMaxTime)
      {
        break;
      }

      // Post-sequence
      sequenceStartTime = 0;
      sequence = SEQUENCE_REGULATING;

    case SEQUENCE_REGULATING:
      // Waiting for reg

      // Pre-sequence
      if (sequenceStartTime == 0) {
        relayOn();
        sequenceStartTime = currentTime;
        elapsedTime = 0;
        ledRed.Off();
        ledBlue.Off();
      }
      else {
        // Calc elapsed time in seconds
        elapsedTime = (currentTime - sequenceStartTime) / 1000;
      }

      ledGreen.Execute(20, 1500);

      // Diagnostic
      stepMaxTime = regulationMaxTime;
      stepElapsedTime = elapsedTime;
      stepMaxValue = refCurrent;
      stepCurValue = min(min(leftA1CurrentAverage, leftA2CurrentAverage), min(rightA1CurrentAverage, rightA2CurrentAverage));

      if (elapsedTime > regulationMaxTime)
      {
        // Fail, too late
        sequence = SEQUENCE_FAIL;
        errorNumber = ERROR_REGULATINGTOOLONG;
        break;
      }

      if (checkInRange(0, maxCurrent) != CHECK_RANGE_OK)
      {
        // Fail current error
        if (errorTime == 0) {
          errorTime = currentTime;
        }

        if (currentTime - errorTime > errorMaxTime)
        {
          // Fail current error
          sequence = SEQUENCE_FAIL;
          errorNumber = ERROR_REGULATINGMAXREACHED;
          break;
        }
      }
      else {
        errorTime = 0;
      }

      //if(checkInRange(refCurrent - stabilizedTreshold, refCurrent + stabilizedTreshold) != CHECK_RANGE_OK)
      if (checkInRange(minCurrent + 10, maxCurrent) != CHECK_RANGE_OK)
      {
        break;
      }

      // Post-sequence
      sequenceStartTime = 0;
      sequence = SEQUENCE_FUNCTION;

    case SEQUENCE_FUNCTION:
      // Normal Function

      // Pre-sequence
      if (sequenceStartTime == 0) {
        relayOn();
        sequenceStartTime = currentTime;
        elapsedTime = 0;
        ledRed.Off();
        ledGreen.Off();
      }
      else {
        // Calc elapsed time in seconds
        elapsedTime = (currentTime - sequenceStartTime) / 1000;
      }

      // Blue color
      ledBlue.On();

      // Measure Temp
      if (currentTime - lastTempMeasureTime > tempMeasureMinTime) {
        measureTemperatures();
      }

      // Diagnostic
      stepMaxTime = 0;
      stepElapsedTime = elapsedTime;
      stepMaxValue = 0;
      stepCurValue = 0;

      check = checkInRange(minCurrent, maxCurrent);
      if (check != CHECK_RANGE_OK)
      {
        if (errorTime == 0) {
          errorTime = currentTime;
        }

        if (currentTime - errorTime > errorMaxTime)
        {
          // Fail current error
          sequence = SEQUENCE_FAIL;
          errorNumber = check == CHECK_RANGE_TOOLOW ? ERROR_FUNCTIONMINREACHED : ERROR_FUNCTIONMAXREACHED;
          break;
        }
      }
      else {
        errorTime = 0;
      }
      break;

    default:
      // Fail, protect mode
      // Ensure relay off
      relayOff();

      // Diagnostic
      stepMaxTime = 0;
      stepElapsedTime = 0;
      stepMaxValue = 0;
      stepCurValue = 0;

      // Error indicator
      ledBlue.Off();
      ledGreen.Off();
      ledRed.Execute(250, errorNumber, 1200);
  }

  // Diagnostic
  if (Serial.available()) {
    char inChar = (char)Serial.read();
    if (inChar == '*') {
      // Start request
      inputString = inChar;
    } else if (inChar == '\r') {
      // Ignored
    } else if (inChar == '\n') {
      request = inputString;
      inputString = "";
    } else {
      inputString += inChar;
    }
  }

  if (request == "*get_d") {
    request = "";
    sendDatas();
  }
}











